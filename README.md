# TemplateTs

Template projeto em nodejs configurado para trabalhar com:
- Typescript: tipagem estática
- Dotenv: variaveis de ambriente em arquivo .env
- ESLint: analise de sintaxe de código
- Prettier: formatação de código
- Vitest: testes
- Husky: configuração e execução de git hooks
- Commitlint: padronização de commits

Ferramentas preparadas para logs no arquivo `src/core/Logger.ts` :
- TSLog: log de erros
- Node Color Log: log de informações, falhas, sucessos, etc...

Ferramenta para tratamento de erros no arquivo `src/core/Either.ts` inspirado na classe `Either` do pacote `fp-ts`.

### Estrutura de pastas:
 .  
├──  .husky  
│  ├──  _  
│  │  ├──  .gitignore  
│  │  └──  husky.sh  
│  └──  commit-msg  
├──  src  
│  ├──  controllers  
│  │  └──  index.ts  
│  ├──  core  
│  │  ├──  Either.ts  
│  │  ├──  index.ts  
│  │  ├──  Logger.ts  
│  │  └──  SignalHandler.ts  
│  ├──  db  
│  ├──  entities  
│  │  └──  index.ts  
│  ├──  errors  
│  │  └──  index.ts  
│  ├──  index.ts  
│  ├──  interfaces  
│  │  ├──  controllers  
│  │  ├──  core  
│  │  ├──  entities  
│  │  ├──  errors  
│  │  ├──  index.ts  
│  │  ├──  repositories  
│  │  └──  services  
│  ├──  modules  
│  │  └──  index.ts  
│  ├──  repositories  
│  │  └──  index.ts  
│  └──  services  
│     └──  index.ts  
├──  tests  
│  └──  metaTest.test.ts  
├──  commitlint.config.js
├──  package.json  
├──  README.md  
├──  .eslintignore  
├──  .eslintrc.json  
├──  .git
├──  .gitignore  
├──  .prettierrc  
├──  tsconfig.json  
└──  vitest.config.ts  

## Como usar o template:

1. O primeiro passo é clonar o repositório:

`git clone git@gitlab.com:ViniciusJO/templatets.git`

ou via `http` :

`git clone https://gitlab.com/ViniciusJO/templatets.git`

2. Em seguida entramos no diretório e instalamos os pacotes: 

`cd templatets`

`yarn` ou `npm install`

3. Então é só rodar o projeto

`yarn start` ou  `npm run start`

4. Para desenvolvimento utilizar o comando a seguir para reinicio automático da aplicação ao alterar algum arquivo:

`yarn dev` ou `npm run dev`
