import { describe, it, expect } from "vitest";

describe("Vitest test", async () => {
	it("should succeed", () => {
		expect(2 + 2).toEqual(4);
	});
	it("should fail", () => {
		expect(2 + 2).toEqual(5);
	});
});
