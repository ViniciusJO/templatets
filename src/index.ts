/* eslint-disable no-constant-condition */
import "dotenv/config";
import { handleSignal, Logger } from "./core";

handleSignal();

Logger.logger.color("yellow").bold().italic().underscore().reverse().log("Teste!");

const main = async () => {
  while (true);
};

main();
