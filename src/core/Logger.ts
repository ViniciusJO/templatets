import logger from "node-color-log";
import { Logger as tsLoggerClass } from "tslog";

export const Logger = { logger, tsLogger: new tsLoggerClass() };
