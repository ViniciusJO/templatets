import { SignalConstants } from "os";
import { Logger } from ".";

export const handleSignal = () => {
	process.on("SIGINT", (signal: SignalConstants) => {
		Logger.logger.color("yellow").bold().italic().underscore().log("Interrupted!");
		process.exit();
	});
	process.on("SIGTERM", (signal: SignalConstants) => {
		Logger.logger.color("blue").bold().italic().underscore().log("Terminated!");
		process.exit();
	});
	process.on("SIGQUIT", (signal: SignalConstants) => {
		Logger.logger.color("blue").bold().italic().underscore().log("Quited!");
		process.exit();
	});
};
