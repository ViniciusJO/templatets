export class Success<F, S> {
	value: S;
	constructor(value: S) {
		this.value = value;
	}

	isSucesso(): this is Success<F, S> {
		return true;
	}

	isFailure(): this is Failure<F, S> {
		return false;
	}
}

export class Failure<F, S> {
	value: F;
	constructor(value: F) {
		this.value = value;
	}
	isSucesso(): this is Success<F, S> {
		return false;
	}

	isFailure(): this is Failure<F, S> {
		return true;
	}
}

export type Either<F, S> = NonNullable<Failure<F, S> | Success<F, S>>;

export const success = <F, S>(s: S): Either<F, S> => {
	return new Success(s);
};

export const failure = <F, S>(f: F): Either<F, S> => {
	return new Failure(f);
};
